package bn.examples.common.criteria.service;

public interface Criteria {

    Criteria copy();

}
