package bn.examples.common.security;

public enum UserAuthority {
    ANONYMOUS,
    USER, //user that has just registered in the web site, not using any services
    MANAGER, //manager who adds, removes, updates course content
    ADMIN, //manages all info in the base
    SUPER_USER;

}
