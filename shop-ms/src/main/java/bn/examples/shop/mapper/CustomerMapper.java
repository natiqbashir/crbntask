package bn.examples.shop.mapper;


import bn.examples.shop.domain.Customer;
import bn.examples.shop.dto.customer.CustomerDto;
import bn.examples.shop.dto.customer.CustomerInfoDto;
import bn.examples.shop.dto.customer.RegisterCustomerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface CustomerMapper {

    CustomerDto entityToDto(Customer customer);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "authorities", ignore = true)
    @Mapping(target = "password", ignore = true)
    Customer dtoToEntity(RegisterCustomerDto dto);

    CustomerInfoDto entityToInfoDto(Customer customer);

}
