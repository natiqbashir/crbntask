package bn.examples.shop.dto.customer;


import bn.examples.shop.service.validation.ValidPassword;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ManagedCustomerDto extends CustomerDto {

    @ValidPassword
    private String password;

}
