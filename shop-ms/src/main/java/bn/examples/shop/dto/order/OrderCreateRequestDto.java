package bn.examples.shop.dto.order;


import bn.examples.shop.domain.OrderDetails;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class OrderCreateRequestDto {

    @NotNull
    private String status;

    @NotNull
    private Long customer;

    @NotNull
    private List<OrderDetailsDto> orderDetails;

}
