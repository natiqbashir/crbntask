package bn.examples.shop.dto.product;

public enum ProductStatus {

    INACTIVE, ACTIVE
}
