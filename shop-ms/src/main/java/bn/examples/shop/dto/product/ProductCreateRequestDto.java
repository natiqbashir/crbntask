package bn.examples.shop.dto.product;


import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ProductCreateRequestDto {

    private String name;

    @Positive
    private Double price;

    @Valid
    private ProductStatus status;
}
