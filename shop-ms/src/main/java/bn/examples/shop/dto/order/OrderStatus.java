package bn.examples.shop.dto.order;

public enum OrderStatus {

    IN_Process, FINISHED, CANCELLED
}
