package bn.examples.shop.dto.product;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ProductResponseDto {

    private Long id;
    private String name;
    private Double price;
    private ProductStatus status;
}
