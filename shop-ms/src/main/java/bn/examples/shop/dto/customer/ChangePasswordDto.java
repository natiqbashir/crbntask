package bn.examples.shop.dto.customer;

import bn.examples.shop.service.validation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordDto {

    @NotNull
    private String oldPassword;

    @NotNull
    @ValidPassword
    private String newPassword;

    @NotNull
    @ValidPassword
    private String repeatPassword;

}
