package bn.examples.shop.dto.customer;

import bn.examples.shop.service.validation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = "password")
public class RegisterCustomerDto {

    @NotNull
    private String name;

    @NotNull
    private String surname;

    @NotNull
    private String username;

    @ValidPassword
    private String password;

}
