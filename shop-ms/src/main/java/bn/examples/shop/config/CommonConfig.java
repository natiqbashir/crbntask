package bn.examples.shop.config;


import bn.examples.common.config.ModelMapperConfig;
import bn.examples.common.security.config.SwaggerCorsConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({SwaggerCorsConfigurer.class, ModelMapperConfig.class})
public class CommonConfig {

}
