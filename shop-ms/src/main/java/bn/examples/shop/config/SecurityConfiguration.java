package bn.examples.shop.config;


import bn.examples.common.security.auth.AuthenticationEntryPointConfigurer;
import bn.examples.common.security.auth.service.JwtService;
import bn.examples.common.security.auth.service.TokenAuthService;
import bn.examples.common.security.config.BaseSecurityConfig;
import bn.examples.common.security.config.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Slf4j
@EnableWebSecurity
@Import({SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class})
//LoggingTcpConfiguration.class
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends BaseSecurityConfig {


    private static final String ORDER_API_V1 = "/v1/orders/**";
    private static final String ORDER_API_V1_SEARCH = "/v1/orders/search";

    private static final String PRODUCT_API_V1 = "/v1/products/**";
    private static final String PRODUCT_API_V1_SEARCH = "/v1/products/search";

    private static final String CUSTOMERS_API_V1 = "/v1/customers/**";
    private static final String CUSTOMERS_API_V1_SEARCH = "/v1/customers/search";


    public SecurityConfiguration(SecurityProperties securityProperties, JwtService jwtService) {
        super(securityProperties, List.of(new TokenAuthService(jwtService)));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(
                        "/authenticate"
                )
                .permitAll()
                .and()
                .authorizeRequests()
                // ORDER_API_V1
                .antMatchers(HttpMethod.GET, ORDER_API_V1).authenticated()
                .antMatchers(HttpMethod.POST, ORDER_API_V1_SEARCH).authenticated()

                // PRODUCT_API_V1
                .antMatchers(HttpMethod.GET, PRODUCT_API_V1).permitAll()
                .antMatchers(HttpMethod.POST, PRODUCT_API_V1_SEARCH).authenticated()

                // CUSTOMERS_API_V1
                .antMatchers(HttpMethod.GET, CUSTOMERS_API_V1).authenticated()
                .antMatchers(HttpMethod.POST, CUSTOMERS_API_V1).permitAll()
                .antMatchers(HttpMethod.POST, CUSTOMERS_API_V1_SEARCH).authenticated();

        super.configure(http);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
