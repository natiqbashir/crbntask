package bn.examples.shop.web.rest;


import bn.examples.common.dto.GenericSearchDto;

import bn.examples.shop.dto.product.ProductCreateRequestDto;
import bn.examples.shop.dto.product.ProductResponseDto;
import bn.examples.shop.dto.product.ProductStatus;
import bn.examples.shop.service.ProductService;
import bn.examples.shop.service.query.criteria.ProductCriteria;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/products")
public class ProductController {

    private final ProductService productService;


    @PostMapping
    public ResponseEntity<ProductResponseDto> create(
            @RequestBody @Valid ProductCreateRequestDto requestDto) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productService.create(requestDto));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseDto> get(@PathVariable Long id) {
        return ResponseEntity.ok(productService.get(id));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseDto> update(@PathVariable Long id,
                                                  @RequestBody ProductCreateRequestDto requestDto) {
        return ResponseEntity.ok(productService.update(id, requestDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Removing product with id {}", id);
        productService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/search")
    public ResponseEntity<Slice<ProductResponseDto>> search(@RequestBody GenericSearchDto genericSearchDto,
                                                            Pageable pageable) {
        log.trace("Search products request");
        return ResponseEntity.ok(productService.search(genericSearchDto, pageable));
    }

    @GetMapping
    public ResponseEntity<Slice<ProductResponseDto>> search(ProductCriteria productCriteria, Pageable pageable) {

        log.trace("Search product request by status and pageable: {}: {}", productCriteria, pageable);

        if (productCriteria.equals(ProductStatus.ACTIVE)) {
            return ResponseEntity.ok(productService.search(productCriteria, pageable));
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
