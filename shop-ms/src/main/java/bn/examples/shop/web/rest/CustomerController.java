package bn.examples.shop.web.rest;


import bn.examples.common.dto.GenericSearchDto;
import bn.examples.shop.dto.customer.CustomerDto;
import bn.examples.shop.dto.customer.RegisterCustomerDto;
import bn.examples.shop.dto.customer.UpdateCustomerDto;
import bn.examples.shop.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/v1/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<Void> register(@RequestBody @Validated RegisterCustomerDto dto) {
        customerService.register(dto);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<CustomerDto> getCurrentCustomer() {
        return ResponseEntity.ok(customerService.getCurrentCustomer());
    }

    @PostMapping("/search")
    public ResponseEntity<Page<CustomerDto>> search(@RequestBody GenericSearchDto genericSearchDto,
                                                    Pageable pageable) {
        return ResponseEntity.ok(customerService.search(genericSearchDto, pageable));
    }

    @PutMapping
    public CustomerDto updateCustomer(@RequestBody @Valid UpdateCustomerDto updateCustomerDto) {
        return customerService.update(updateCustomerDto);
    }


}
