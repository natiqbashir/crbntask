package bn.examples.shop.web.rest;


import bn.examples.common.dto.GenericSearchDto;
import bn.examples.shop.dto.order.OrderCreateRequestDto;
import bn.examples.shop.dto.order.OrderResponseDto;
import bn.examples.shop.dto.order.OrderStatus;
import bn.examples.shop.service.OrderService;
import bn.examples.shop.service.query.criteria.OrderCriteria;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/orders")
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/{id}")
    public ResponseEntity<OrderResponseDto> get(@PathVariable Long id) {
        log.trace("Get order request {}", id);
        return ResponseEntity.ok(orderService.get(id));
    }

    @PostMapping
    public ResponseEntity<OrderResponseDto> create(@RequestBody @Valid OrderCreateRequestDto createRequestDto) {
        log.trace("Create order request {}", createRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(orderService.create(createRequestDto));

    }

    @PutMapping("/status/{id}")
    public ResponseEntity<OrderResponseDto> update(@PathVariable Long id,
                                                  @RequestParam String status) {
        return ResponseEntity.ok(orderService.updateStatus(id, status));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Delete order {} request", id);
        orderService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/search")
    public Page<OrderResponseDto> search(@RequestBody GenericSearchDto dto, Pageable pageable) {
        log.trace("Search order request");
        return orderService.search(dto, pageable);
    }

    @GetMapping
    public ResponseEntity<Slice<OrderResponseDto>> search(OrderCriteria orderCriteria, Pageable pageable) {

        log.trace("Search order request by status and pageable: {}: {}", orderCriteria, pageable);

        if (orderCriteria.equals(OrderStatus.FINISHED)) {
            return ResponseEntity.ok(orderService.search(orderCriteria, pageable));
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
