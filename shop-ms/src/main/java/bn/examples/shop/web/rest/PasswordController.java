package bn.examples.shop.web.rest;


import bn.examples.shop.dto.customer.ChangePasswordDto;
import bn.examples.shop.service.PasswordService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RestController
@RequestMapping("/customer/password")
@RequiredArgsConstructor
public class PasswordController {

    private final PasswordService passwordService;

    @PutMapping("/change")
    public void changePassword(@RequestBody @Valid ChangePasswordDto dto) {
        passwordService.changePassword(dto);
    }

}
