package bn.examples.shop.exception;


import bn.examples.common.exception.NotFoundException;

public class BalanceNotEnoughException extends NotFoundException {

    public static final String MESSAGE = "Balance is not enough.";
    private static final long serialVersionUID = 5843213248811L;

    public BalanceNotEnoughException() {
        super(MESSAGE);
    }
}
