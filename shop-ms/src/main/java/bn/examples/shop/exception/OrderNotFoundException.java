package bn.examples.shop.exception;


import bn.examples.common.exception.NotFoundException;

public class OrderNotFoundException extends NotFoundException {

    public static final String MESSAGE = "Order %s does not exist.";
    private static final long serialVersionUID = 5843213248811L;

    public OrderNotFoundException(Long orderId) {
        super(String.format(MESSAGE, orderId));
    }
}
