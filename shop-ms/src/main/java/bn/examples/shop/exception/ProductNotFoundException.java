package bn.examples.shop.exception;


import bn.examples.common.exception.NotFoundException;

public class ProductNotFoundException extends NotFoundException {

    public static final String MESSAGE = "Product %s does not exist.";
    private static final long serialVersionUID = 5843213248811L;

    public ProductNotFoundException(Long productId) {
        super(String.format(MESSAGE, productId));
    }
}
