package bn.examples.shop.exception;


import bn.examples.common.exception.NotFoundException;

public class CustomerNotFoundException extends NotFoundException {

    private static final long serialVersionUID = 58432132465811L;

    public CustomerNotFoundException() {
        super("Customer not found");
    }
    public CustomerNotFoundException(String msg) {
        super(msg);
    }
}
