package bn.examples.shop.exception;


import bn.examples.common.exception.NotFoundException;

public class OrderStatusNotFoundException extends NotFoundException {

    public static final String MESSAGE = "In order status there is no status like this %s.";
    private static final long serialVersionUID = 5843213248811L;

    public OrderStatusNotFoundException(String status) {
        super(String.format(MESSAGE, status));
    }

}
