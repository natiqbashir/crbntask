package bn.examples.shop.exception;


import bn.examples.common.exception.ServiceUnavailableException;

public class KafkaPublisherException extends ServiceUnavailableException {

    private static final long serialVersionUID = 8793120399511589939L;

}
