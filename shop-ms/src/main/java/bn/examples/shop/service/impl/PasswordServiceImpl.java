package bn.examples.shop.service.impl;


import bn.examples.common.security.auth.service.SecurityService;
import bn.examples.shop.dto.customer.ChangePasswordDto;
import bn.examples.shop.exception.NewPasswordsNotMatchException;
import bn.examples.shop.exception.PasswordIsIncorrectException;
import bn.examples.shop.repository.CustomerRepository;
import bn.examples.shop.service.PasswordService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Import(SecurityService.class)
@RequiredArgsConstructor
public class PasswordServiceImpl implements PasswordService {

    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;
    private final SecurityService securityService;


    @Override
    public void changePassword(ChangePasswordDto dto) {
        Optional<String> currentCustomerLogin = securityService.getCurrentCustomerLogin();
        customerRepository.findByUsername(currentCustomerLogin.get()).ifPresent(
                customer -> {
                    if (!passwordEncoder.matches(dto.getOldPassword(), customer.getPassword())) {
                        throw new PasswordIsIncorrectException();
                    }
                    checkIfNewPasswordsMatch(dto.getNewPassword(), dto.getRepeatPassword());
                    customer.setPassword(passwordEncoder.encode(dto.getNewPassword()));
                    customerRepository.save(customer);
                });
    }

    private void checkIfNewPasswordsMatch(String password, String repeatPassword) {
        if (!password.equals(repeatPassword)) {
            throw new NewPasswordsNotMatchException();
        }
    }


}
