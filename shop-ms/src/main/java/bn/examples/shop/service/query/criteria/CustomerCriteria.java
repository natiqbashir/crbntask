package bn.examples.shop.service.query.criteria;


import bn.examples.common.criteria.filter.LongFilter;
import bn.examples.common.criteria.filter.StringFilter;
import bn.examples.common.criteria.service.Criteria;
import lombok.Data;

@Data
public class CustomerCriteria implements Criteria {

    private LongFilter id;
    private StringFilter name;
    private StringFilter surname;
    private StringFilter username;


    @Override
    public Criteria copy() {
        return this;
    }
}
