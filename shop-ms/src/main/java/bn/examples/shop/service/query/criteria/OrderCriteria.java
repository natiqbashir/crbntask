package bn.examples.shop.service.query.criteria;


import bn.examples.common.criteria.filter.LongFilter;
import bn.examples.common.criteria.filter.RangeFilter;
import bn.examples.common.criteria.filter.StringFilter;
import bn.examples.common.criteria.service.Criteria;
import lombok.Data;

@Data
public class OrderCriteria implements Criteria {

    private LongFilter id;
    private StringFilter status;
    private RangeFilter finishDate;

    @Override
    public Criteria copy() {
        return this;
    }
}
