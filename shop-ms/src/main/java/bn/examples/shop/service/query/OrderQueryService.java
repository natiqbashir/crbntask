package bn.examples.shop.service.query;


import bn.examples.common.criteria.service.QueryService;
import bn.examples.shop.domain.Order;
import bn.examples.shop.domain.Order_;
import bn.examples.shop.service.query.criteria.OrderCriteria;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class OrderQueryService extends QueryService<Order> {
    @SuppressWarnings("MethodLength")
    public Specification<Order> createSpecification(OrderCriteria taskCriteria) {
        Specification<Order> specification = Specification.where(null);
        if (Objects.nonNull(taskCriteria)) {
            if (Objects.nonNull(taskCriteria.getId())) {
                specification = specification.and(buildSpecification(taskCriteria.getId(), Order_.id));
            }
            if (Objects.nonNull(taskCriteria.getStatus())) {
                specification = specification.and(buildStringSpecification(taskCriteria.getStatus(), Order_.status));
            }

        }

        return specification;
    }

}
