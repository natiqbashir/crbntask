package bn.examples.shop.service.impl;

import bn.examples.common.dto.GenericSearchDto;
import bn.examples.common.exception.NotFoundException;
import bn.examples.common.search.SearchSpecification;

import bn.examples.shop.domain.Product;
import bn.examples.shop.dto.product.ProductCreateRequestDto;
import bn.examples.shop.dto.product.ProductResponseDto;
import bn.examples.shop.dto.product.ProductStatus;
import bn.examples.shop.exception.ProductNotFoundException;
import bn.examples.shop.repository.ProductRepository;
import bn.examples.shop.service.ProductService;
import bn.examples.shop.service.query.ProductQueryService;
import bn.examples.shop.service.query.criteria.ProductCriteria;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ModelMapper modelMapper;
    private final ProductRepository productRepository;
    private final ProductQueryService productQueryService;


    @Override
    @Transactional
    public ProductResponseDto create(ProductCreateRequestDto requestDto) {
        Product product = modelMapper.map(requestDto, Product.class);
        return modelMapper.map(productRepository.save(product), ProductResponseDto.class);
    }

    @Override
    public ProductResponseDto get(Long id) {
        return productRepository.findById(id)
                .map(product -> modelMapper.map(product, ProductResponseDto.class))
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    @Override
    public ProductResponseDto update(Long id, ProductCreateRequestDto requestDto) {
        return productRepository
                .findById(id).map(product -> {
                    modelMapper.map(requestDto, product);
                    product.setName(requestDto.getName());
                    product.setPrice(requestDto.getPrice());
                    product.setStatus((requestDto.getStatus()));
                    return modelMapper.map(productRepository.save(product), ProductResponseDto.class);
                })
                .orElseThrow(() -> new ProductNotFoundException(id));
    }

    @Override
    public void delete(Long id) {
        log.trace("Remove product with id {}", id);
        Product product = productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
        productRepository.delete(product);
    }

    @Override
    public Page<ProductResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return productRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(product -> modelMapper.map(product, ProductResponseDto.class));
    }

    @Override
    public Page<ProductResponseDto> search(ProductCriteria productCriteria, Pageable pageable) {
        Specification<Product> specification = productQueryService.createSpecification(productCriteria);
        return productRepository
                .findAll(specification, pageable)
                .map(product -> modelMapper.map(product, ProductResponseDto.class));
    }


}
