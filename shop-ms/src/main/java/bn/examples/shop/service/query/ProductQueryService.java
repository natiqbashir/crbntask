package bn.examples.shop.service.query;


import bn.examples.common.criteria.service.QueryService;
import bn.examples.shop.domain.Product;
import bn.examples.shop.domain.Product_;
import bn.examples.shop.service.query.criteria.ProductCriteria;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class ProductQueryService extends QueryService<Product> {
    @SuppressWarnings("MethodLength")
    public Specification<Product> createSpecification(ProductCriteria taskCriteria) {
        Specification<Product> specification = Specification.where(null);
        if (Objects.nonNull(taskCriteria)) {
            if (Objects.nonNull(taskCriteria.getId())) {
                specification = specification.and(buildSpecification(taskCriteria.getId(), Product_.id));
            }
            if (Objects.nonNull(taskCriteria.getName())) {
                specification = specification.and(buildStringSpecification(taskCriteria.getName(), Product_.name));
            }
            if (Objects.nonNull(taskCriteria.getPrice())) {
                specification =
                        specification.and(buildSpecification(taskCriteria.getPrice(), Product_.price));
            }


        }

        return specification;
    }

}
