package bn.examples.shop.service;

import bn.examples.common.dto.GenericSearchDto;


import bn.examples.shop.dto.product.ProductCreateRequestDto;
import bn.examples.shop.dto.product.ProductResponseDto;
import bn.examples.shop.service.query.criteria.ProductCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {

    ProductResponseDto create(ProductCreateRequestDto requestDto);

    ProductResponseDto get(Long id);

    ProductResponseDto update(Long id, ProductCreateRequestDto requestDto);

    void delete(Long id);

    Page<ProductResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable);

    Page<ProductResponseDto> search(ProductCriteria productCriteria, Pageable pageable);
}
