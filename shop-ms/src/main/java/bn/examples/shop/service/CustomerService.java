package bn.examples.shop.service;


import bn.examples.common.dto.GenericSearchDto;
import bn.examples.shop.dto.customer.CustomerDto;
import bn.examples.shop.dto.customer.RegisterCustomerDto;
import bn.examples.shop.dto.customer.UpdateCustomerDto;

import bn.examples.shop.service.query.criteria.CustomerCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CustomerService {

    void register(RegisterCustomerDto customerVm);

    CustomerDto getCurrentCustomer();

    Page<CustomerDto> search(GenericSearchDto genericSearchDto, Pageable pageable);

    Page<CustomerDto> search(CustomerCriteria customerCriteria, Pageable pageable);

    CustomerDto update(UpdateCustomerDto updateCustomerDto);


}
