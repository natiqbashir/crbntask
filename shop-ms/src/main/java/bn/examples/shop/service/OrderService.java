package bn.examples.shop.service;

import bn.examples.common.dto.GenericSearchDto;
import bn.examples.shop.dto.order.OrderCreateRequestDto;
import bn.examples.shop.dto.order.OrderResponseDto;
import bn.examples.shop.service.query.criteria.OrderCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface OrderService {

    OrderResponseDto create(OrderCreateRequestDto requestDto);

    OrderResponseDto get(Long id);

    OrderResponseDto updateStatus(Long id, String status);

    void delete(Long id);

    Page<OrderResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable);

    Page<OrderResponseDto> search(OrderCriteria orderCriteria, Pageable pageable);
}
