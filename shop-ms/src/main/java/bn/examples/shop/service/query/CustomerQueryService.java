package bn.examples.shop.service.query;


import bn.examples.common.criteria.service.QueryService;
import bn.examples.shop.domain.Customer;
import bn.examples.shop.domain.Customer_;
import bn.examples.shop.service.query.criteria.CustomerCriteria;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
public class CustomerQueryService extends QueryService<Customer> {
    @SuppressWarnings("MethodLength")
    public Specification<Customer> createSpecification(CustomerCriteria taskCriteria) {
        Specification<Customer> specification = Specification.where(null);
        if (Objects.nonNull(taskCriteria)) {
            if (Objects.nonNull(taskCriteria.getId())) {
                specification = specification.and(buildSpecification(taskCriteria.getId(), Customer_.id));
            }
            if (Objects.nonNull(taskCriteria.getName())) {
                specification = specification.and(buildStringSpecification(taskCriteria.getName(), Customer_.name));
            }
            if (Objects.nonNull(taskCriteria.getSurname())) {
                specification =
                        specification.and(buildStringSpecification(taskCriteria.getSurname(), Customer_.surname));
            }
            if (Objects.nonNull(taskCriteria.getUsername())) {
                specification =
                        specification.and(buildStringSpecification(taskCriteria.getUsername(), Customer_.username));
            }

        }

        return specification;
    }

}
