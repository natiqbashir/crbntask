package bn.examples.shop.service.query.criteria;


import bn.examples.common.criteria.filter.DoubleFilter;
import bn.examples.common.criteria.filter.LongFilter;
import bn.examples.common.criteria.filter.StringFilter;
import bn.examples.common.criteria.service.Criteria;
import lombok.Data;

@Data
public class ProductCriteria implements Criteria {

    private LongFilter id;
    private StringFilter name;
    private DoubleFilter price;


    @Override
    public Criteria copy() {
        return this;
    }
}
