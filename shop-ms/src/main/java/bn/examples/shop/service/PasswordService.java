package bn.examples.shop.service;


import bn.examples.shop.dto.customer.ChangePasswordDto;

public interface PasswordService {

    void changePassword(ChangePasswordDto dto);

}
