package bn.examples.shop.service.impl;


import bn.examples.common.dto.GenericSearchDto;
import bn.examples.common.search.SearchSpecification;
import bn.examples.common.security.auth.service.SecurityService;
import bn.examples.shop.domain.Authority;
import bn.examples.shop.domain.Customer;
import bn.examples.shop.domain.Product;
import bn.examples.shop.dto.customer.CustomerDto;
import bn.examples.shop.dto.customer.RegisterCustomerDto;
import bn.examples.shop.dto.customer.UpdateCustomerDto;
import bn.examples.shop.dto.product.ProductResponseDto;
import bn.examples.shop.exception.CustomerNotFoundException;
import bn.examples.shop.exception.UsernameAlreadyUsedException;
import bn.examples.shop.mapper.CustomerMapper;
import bn.examples.shop.repository.CustomerRepository;
import bn.examples.shop.service.CustomerService;
import bn.examples.shop.service.query.CustomerQueryService;
import bn.examples.shop.service.query.criteria.CustomerCriteria;
import bn.examples.shop.service.query.criteria.ProductCriteria;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import static bn.examples.common.security.UserAuthority.USER;


@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final PasswordEncoder passwordEncoder;
    private final CustomerMapper customerMapper;
    private final CustomerQueryService customerQueryService;
    private final SecurityService securityService;
    private final ModelMapper modelMapper;


    @Override
    public void register(RegisterCustomerDto dto) {
        customerRepository.findByUsername(dto.getUsername())
                .ifPresent(customer -> {
                    throw new UsernameAlreadyUsedException(dto.getUsername());
                });
        Customer customer = createCustomerEntityObject(dto, createAuthorities(USER.name()));
        customerRepository.save(customer);
    }



    @Override
    public CustomerDto update(UpdateCustomerDto updateCustomerDto) {
        CustomerDto currentCustomer = getCurrentCustomer();
        Customer byId = customerRepository.findById(currentCustomer.getId())
                .orElseThrow(CustomerNotFoundException::new);
        byId.setName(updateCustomerDto.getName());
        byId.setSurname(updateCustomerDto.getSurname());
        return customerMapper.entityToDto(customerRepository.save(byId));
    }

    @Override
    public CustomerDto getCurrentCustomer() {
        return customerMapper.entityToDto(getCurrentCustomerByUsername(getCurrentLogin()));
    }

    @Override
    public Page<CustomerDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return customerRepository.findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(customerMapper::entityToDto);
    }

    @Override
    public Page<CustomerDto> search(CustomerCriteria customerCriteria, Pageable pageable) {
        Specification<Customer> specification = customerQueryService.createSpecification(customerCriteria);
        return customerRepository
                .findAll(specification, pageable)
                .map(customer -> modelMapper.map(customer, CustomerDto.class));
    }

    private Set<Authority> createAuthorities(String... authoritiesString) {
        Set<Authority> authorities = new HashSet<>();
        for (String authorityString : authoritiesString) {
            authorities.add(new Authority(authorityString));
        }
        return authorities;
    }

    private Customer createCustomerEntityObject(RegisterCustomerDto registerCustomerDto, Set<Authority> authorities) {
        Customer customer = customerMapper.dtoToEntity(registerCustomerDto);
        customer.setPassword(passwordEncoder.encode(registerCustomerDto.getPassword()));
        customer.setAuthorities(authorities);
        return customer;
    }

    private Customer getCurrentCustomerByUsername(String username) {
        return customerRepository.findByUsername(username).orElseThrow(CustomerNotFoundException::new);
    }

    private String getCurrentLogin() {
        return securityService.getCurrentCustomerLogin().orElseThrow(CustomerNotFoundException::new);
    }

}
