package bn.examples.shop.service.impl;

import static bn.examples.shop.dto.order.OrderStatus.CANCELLED;
import static bn.examples.shop.dto.order.OrderStatus.FINISHED;
import static bn.examples.shop.dto.order.OrderStatus.IN_Process;

import bn.examples.common.dto.GenericSearchDto;
import bn.examples.common.search.SearchSpecification;
import bn.examples.common.security.auth.service.SecurityService;
import bn.examples.shop.domain.Customer;
import bn.examples.shop.domain.Order;
import bn.examples.shop.domain.OrderDetails;
import bn.examples.shop.domain.Product;
import bn.examples.shop.dto.order.OrderCreateRequestDto;
import bn.examples.shop.dto.order.OrderResponseDto;
import bn.examples.shop.dto.order.OrderStatus;
import bn.examples.shop.exception.BalanceNotEnoughException;
import bn.examples.shop.exception.CustomerNotFoundException;
import bn.examples.shop.exception.OrderNotFoundException;
import bn.examples.shop.exception.ProductNotFoundException;
import bn.examples.shop.repository.CustomerRepository;
import bn.examples.shop.repository.OrderRepository;
import bn.examples.shop.repository.ProductRepository;
import bn.examples.shop.service.OrderService;
import bn.examples.shop.service.query.OrderQueryService;
import bn.examples.shop.service.query.criteria.OrderCriteria;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicReference;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final CustomerRepository customerRepository;
    private final ModelMapper modelMapper;
    private final OrderQueryService orderQueryService;
    private final SecurityService securityService;

    @Override
    @Transactional
    public OrderResponseDto create(OrderCreateRequestDto orderRequestDto) {
        String username = getUsername();
        Customer customer = getCustomer(username);

        Order order = modelMapper.map(orderRequestDto, Order.class);
        order.setStatus(IN_Process.toString());
        order.setCustomer(customer);

        AtomicReference<Double> totalPrice = new AtomicReference<>(0.0);
        orderRequestDto.getOrderDetails().forEach(orderDetails -> {
            Product product = productRepository.findById(orderDetails.getProductId())
                    .orElseThrow(() -> new ProductNotFoundException(orderDetails.getProductId()));
            OrderDetails details = OrderDetails.builder()
                    .order(order)
                    .quantity(orderDetails.getQuantity())
                    .product(product)
                    .build();
            order.getOrderDetails().add(details);
            totalPrice.updateAndGet(v -> v + product.getPrice() * orderDetails.getQuantity());
        });
        if (customer.getBalance() < totalPrice.get()) {
            throw new BalanceNotEnoughException();
        }
        order.setTotalPrice(totalPrice.get());
        return modelMapper.map(orderRepository.save(order), OrderResponseDto.class);
    }

    private Customer getCustomer(String username) {
        return customerRepository.findByUsername(username).orElseThrow(CustomerNotFoundException::new);
    }

    private String getUsername() {
        return securityService.getCurrentCustomerLogin().orElseThrow(CustomerNotFoundException::new);
    }


    @Override
    @Transactional(readOnly = true)
    public OrderResponseDto get(Long id) {
        return orderRepository
                .findById(id)
                .map(order -> modelMapper.map(order, OrderResponseDto.class))
                .orElseThrow(() -> new OrderNotFoundException(id));
    }


    @Override
    public OrderResponseDto updateStatus(Long id, String status) {
        Order order =
                orderRepository.findByCustomer_UsernameAndStatusAndId(getUsername(), IN_Process.name(), id)
                        .orElseThrow(() -> new OrderNotFoundException(id));
        switch (OrderStatus.valueOf(status)) {
            case FINISHED:
                order.setStatus(FINISHED.name());
                order.setFinishDate(LocalDateTime.now());
                orderRepository.save(order);
                return modelMapper.map(order, OrderResponseDto.class);
            case CANCELLED:
                order.setStatus(CANCELLED.name());
                Customer customer = getCustomer(getUsername());
                customer.setBalance(customer.getBalance() + order.getTotalPrice());
                customerRepository.save(customer);
                orderRepository.save(order);
                return modelMapper.map(order, OrderResponseDto.class);
            default:
                return OrderResponseDto.builder().build();
        }
    }


    @Override
    public void delete(Long id) {
        orderRepository.delete(orderRepository.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id)));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrderResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return orderRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(order -> modelMapper.map(order, OrderResponseDto.class));
    }

    @Override
    public Page<OrderResponseDto> search(OrderCriteria orderCriteria, Pageable pageable) {
        Specification<Order> specification = orderQueryService.createSpecification(orderCriteria);
        return orderRepository
                .findAll(specification, pageable)
                .map(order -> modelMapper.map(order, OrderResponseDto.class));
    }

}
